#!/bin/sh

env

# Wait for dependencies to come alive
while true
do

  ping -c1 -w1 wordpress > /dev/null 2> /dev/null
  if [ "$?" != "0" ]
  then
    echo "Waiting for wordpress to come alive.."
    sleep 5
    continue
  fi

  echo "WordPress is awake, let's go!"
  break
done

mkdir -p /etc/certs
mkdir -p /var/www/letsencrypt

# If letsencrypt is providing certs, use those
if ls /etc/letsencrypt/live/$DOMAINNAME/privkey.pem > /dev/null 2> /dev/null
then
  echo "Found letsencrypt certs for $DOMAINNAME, using those"
  ln -sf /etc/letsencrypt/live/$DOMAINNAME/privkey.pem /etc/certs/privkey.pem
  ln -sf /etc/letsencrypt/live/$DOMAINNAME/fullchain.pem /etc/certs/fullchain.pem

# If I loaded devcerts, use those
elif ls /etc/devcerts/site.key > /dev/null 2> /dev/null
then
  echo "Found dev certs for $DOMAINNAME, using those"
  ln -sf /etc/devcerts/site.key /etc/certs/privkey.pem
  ln -sf /etc/devcerts/site.crt /etc/certs/fullchain.pem

# Otherwise, use certbot to get new certs
else
  echo "Couldn't find certs for $DOMAINNAME, initializing those now.."

  certbot certonly --standalone -m $EMAIL --agree-tos --no-eff-email -d $DOMAINNAME -n
  [ $? -eq 0 ] || sleep 9999 # FREEZE! Don't pester eff so much we get throttled

  ln -sf /etc/letsencrypt/live/$DOMAINNAME/privkey.pem /etc/certs/privkey.pem
  ln -sf /etc/letsencrypt/live/$DOMAINNAME/fullchain.pem /etc/certs/fullchain.pem
  echo "Done initializing certs, starting nginx..."

fi

sed -i 's/$hostname/'"$DOMAINNAME"'/' /etc/nginx/nginx.conf

# periodically fork off & see if our certs need to be renewed
renewcerts() {
  while true
  do
    echo -n "Preparing to renew certs... "
    if ls /etc/letsencrypt/live/$DOMAINNAME &> /dev/null
    then
      echo -n "Found certs to renew for $DOMAINNAME... "
      certbot renew --webroot -w /var/www/letsencrypt/ -n
      echo "Done!"
    fi
    echo "No certs found, Done!"
    sleep 48h
  done
}
renewcerts &

sleep 1 # give renewcerts a sec to do it's first check
echo "Entrypoint exiting, starting nginx..."; echo
exec nginx
