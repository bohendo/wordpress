#!/bin/bash

err() { echo $1; exit 1; } # Error out more succinctly

bn=blog_backup # basename of backup bundle & contents

[[ -f "$bn.tar.gz" ]] || err "Error: can't find backup bundle $bn.tar.gz"
[[ -f "deploy.sh" ]] || err "Error: can't find deploy.sh script"

echo "Make sure deploy.sh will use the appropriate env vars"
echo "Current env: DOMAINNAME=$DOMAINNAME EMAIL=$EMAIL"
sleep 2

# Setup environment
set -e 
sudo true
me=`whoami`

echo -n "Unpacking backup bundle..."
tar -xzf $bn.tar.gz

########################################
echo "Done"; echo -n "Restoring PHP plugins... "

# We're going to replace some wordpress files, make sure wordpress isn't alive
if [[ -n "`docker service ps -q blog_wordpress 2> /dev/null`" ]]
then
  echo -n "Removing blog_wordpress service... "
  docker service rm blog_wordpress
  sleep 2
fi

wp=blog_wp_data # name of volume that stores php files
if [[ -z "`docker volume ls -q -f name=$wp`" ]]
then
  echo -n "Creating docker volume called $wp... "
  docker volume create $wp > /dev/null
fi

sudo rm -rf /var/lib/docker/volumes/$wp/_data
sudo mv $bn/$wp/_data /var/lib/docker/volumes/$wp/_data

########################################
echo "Done"; echo -n "Restoring SSL certs... "

# We're going to replace ssl certs, make sure our proxy isn't awake
if [[ -n "`docker service ps -q blog_proxy 2> /dev/null`" ]]
then
  echo -n "Removing blog_proxy service... "
  docker service rm blog_proxy
  sleep 2
fi

le=blog_letsencrypt # name of volume that stores ssl certs
if [[ -z "`docker volume ls -q -f name=$le`" ]]
then
  echo -n "Creating docker volume called $le... "
  docker volume create $le > /dev/null
fi

sudo rm -rf /var/lib/docker/volumes/$le/_data
sudo mv $bn/$le/_data /var/lib/docker/volumes/$le/_data

########################################
echo "Done"; echo -n "Redeploying blog..."

bash deploy.sh > /dev/null

echo "Done"; echo -n "Waiting for MySQL to wake up..."

# runs & returns result of uptime query against mysql container
get_uptime() {
  id=`for f in $(docker service ps -q blog_mysql)
  do
    docker inspect --format '{{.Status.ContainerStatus.ContainerID}}' $f
  done | head -n1`
  q='mysql -u wordpress -p`cat /run/secrets/wp_mysql` wordpress <<<"show global status like '\''Uptime'\'';"'
  docker exec $id bash -c "$q" 2> /dev/null | tail -n1 | tr -dc '0-9'
}

# wait until uptime query returns a number greater than zero
while [[ "`get_uptime 2> /dev/null`" -lt "1" ]]
do
  sleep 3
done

########################################
echo "Done"; echo -n "Restoring MySQL database..."

sudo mv $bn/$bn.sql /var/lib/docker/volumes/blog_mysql_data/_data/

id=`for f in $(docker service ps -q blog_mysql)
do
  docker inspect --format '{{.Status.ContainerStatus.ContainerID}}' $f
done | head -n1`

docker exec $id bash -c 'mysql -u wordpress -p`cat /run/secrets/wp_mysql` wordpress < /var/lib/mysql/'"$bn"'.sql' 2> /dev/null

########################################
echo "Done"; echo -n "Cleaning up..."

rm -rf $bn

echo "Done"
